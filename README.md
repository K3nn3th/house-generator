## House Generator

## Description


This Program displays a random number of houses in OpenGL.
These houses appear to "grow" out of the ground and disappear again, repeatedly.
The animation cycle does the following:

1. takes one second to let all of the houses appear completely (the highest one will take one second to be fully displayed),
2. displays them for three seconds, and 
3. lets them disappear again within one second.

In between the cycles, all of the houses are assigned new random dimensional attribute values which are displayed in the next cycle.

Developed using 
- G++ 13.2.1
- Glew 2.2.0,
- GLFW 3.3.8

## Visuals
![Houses rising](https://i.ibb.co/KVHg6tv/2023-10-26-185826-1920x1080-scrot.png)
![Houses fully displayed](https://i.ibb.co/hdMbcZ5/2023-10-26-185828-1920x1080-scrot.png)

## Installation
- Download the source file and the Makefile and place them inside the same directory.

- `cd` into the directory in a terminal and run `make`

## Usage
simply run the compiled program.

## Authors and acknowledgment
Kenneth Allan

## License
Free
